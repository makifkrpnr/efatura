﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hesaptutar.com__Yeni_
{
    public partial class yeni_irsaliye : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CreateTable();
                AddRow();
                LoadGrid();
                ViewState["MyTable"] = dt;
            }
            else
            {
                dt = (DataTable)ViewState["MyTable"];
            }
        }

        //---------------GridView Satır Ekleme İşlemleri----------------------------
        void CreateTable()
        {
            dt.Columns.Add(new DataColumn("UrunAdi", typeof(string)));
            dt.Columns.Add(new DataColumn("Miktar", typeof(string)));
            dt.Columns.Add(new DataColumn("Birim", typeof(string)));
        }
        void AddRow()
        {
            DataRow dr = dt.NewRow();
            dr["UrunAdi"] = "";
            dr["Miktar"] = "1";
            dr["Birim"] = "";
            dt.Rows.Add(dr);

        }
        void LoadGrid()
        {
            // dgwUrunler.Columns.Clear();
            dgwUrunler.DataSource = dt;
            dgwUrunler.DataBind();

        }
        void SaveData()
        {
            // move grid changes back to table
            foreach (GridViewRow gvRow in dgwUrunler.Rows)
            {
                //extract the TextBox values
                DataRow dr = dt.Rows[gvRow.RowIndex];
                dr["UrunAdi"] = ((TextBox)gvRow.FindControl("txtUrunAdi")).Text;
                dr["Miktar"] = ((TextBox)gvRow.FindControl("txtMiktar")).Text;
                dr["Birim"] = ((DropDownList)gvRow.FindControl("ddlBirim")).Text;
            }
            dt.AcceptChanges();
        }
        protected void cmdDel_Click(object sender, EventArgs e)
        {
            SaveData();
            LinkButton cmdDel = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)cmdDel.Parent.Parent;

            dt.Rows[gvRow.RowIndex].Delete();
            dt.AcceptChanges();
            LoadGrid();
        }
        protected void lbSatirEkle_Click(object sender, EventArgs e)
        {
            SaveData();
            AddRow();
            LoadGrid();
        }
        protected void dgwUrunler_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var dropdown = (DropDownList)e.Row.FindControl("ddlBirim");

                dropdown.SelectedValue = DataBinder.Eval(e.Row.DataItem, "Birim").ToString();//yeni satır eklendiği zaman seçili olan value değeri korunuyor

            }
        }
        //---------------GridView Satır Ekleme İşlemleri----------------------------

    }
}