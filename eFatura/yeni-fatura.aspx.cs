﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace hesaptutar.com__Yeni_
{
    public partial class yeni_fatura : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CreateTable();
                AddRow();
                LoadGrid();
                ViewState["MyTable"] = dt;
                txtIskontoTutari.Text = "0,00";
                txtTahsilEdilen.Text = "0,00";
                String strDolar = fnDoviz_Kurlari("dolar_alis");
                String strEuro = fnDoviz_Kurlari("euro_alis");
                String strSterlin = fnDoviz_Kurlari("sterlin_alis");
                if (ddlParaBirimi.SelectedItem.Text == "USD")
                {
                    txtKur.Text = strDolar;
                }
                else if (ddlParaBirimi.SelectedItem.Text == "EUR")
                {
                    txtKur.Text = strEuro;
                }
                else if (ddlParaBirimi.SelectedItem.Text == "GBP")
                {
                    txtKur.Text = strSterlin;
                }
                lblUSD.Text = strDolar;
                lblEUR.Text = strEuro;
                lblGBP.Text = strSterlin;
                GenelHesap();
            }
            else
            {
                dt = (DataTable)ViewState["MyTable"];
            }
        }
        
        //---------------GridView Satır Ekleme İşlemleri----------------------------
        void CreateTable()
        {
            dt.Columns.Add(new DataColumn("UrunAdi", typeof(string)));
            dt.Columns.Add(new DataColumn("Miktar", typeof(string)));
            dt.Columns.Add(new DataColumn("Birim", typeof(string)));
            dt.Columns.Add(new DataColumn("BirimFiyat", typeof(string)));
            dt.Columns.Add(new DataColumn("ParaBirimi", typeof(string)));
            dt.Columns.Add(new DataColumn("KDV", typeof(string)));
            dt.Columns.Add(new DataColumn("Toplam", typeof(string)));
            dt.Columns.Add(new DataColumn("OTV", typeof(string)));
            dt.Columns.Add(new DataColumn("otvDurum", typeof(string)));
            dt.Columns.Add(new DataColumn("OIV", typeof(string)));
            dt.Columns.Add(new DataColumn("oivDurum", typeof(string)));
            dt.Columns.Add(new DataColumn("OTVGirisTuru", typeof(string)));
        }
        void AddRow()
        {
            DataRow dr = dt.NewRow();
            dr["UrunAdi"] = "";
            dr["Miktar"] = "1";
            dr["Birim"] = "";
            dr["BirimFiyat"] = "";
            if (ddlParaBirimi.SelectedItem.Text == "TRY")
            {
                dr["ParaBirimi"] = "₺";
            }
            else if (ddlParaBirimi.SelectedItem.Text == "USD")
            {
                dr["ParaBirimi"] = "$";
            }
            else if (ddlParaBirimi.SelectedItem.Text == "EUR")
            {
                dr["ParaBirimi"] = "€";
            }
            else if (ddlParaBirimi.SelectedItem.Text == "GBP")
            {
                dr["ParaBirimi"] = "£";
            }

            dr["KDV"] = "";
            dr["Toplam"] = "";
            dr["OTV"] = "";
            dr["otvDurum"] = "0";
            dr["OIV"] = "";
            dr["oivDurum"] = "0";
            dr["OTVGirisTuru"] = "";
            dt.Rows.Add(dr);

        }
        void LoadGrid()
        {
            // dgwUrunler.Columns.Clear();
            dgwUrunler.DataSource = dt;
            dgwUrunler.DataBind();

        }
        void SaveData()
        {
            // move grid changes back to table
            foreach (GridViewRow gvRow in dgwUrunler.Rows)
            {
                //extract the TextBox values
                DataRow dr = dt.Rows[gvRow.RowIndex];
                dr["UrunAdi"] = ((TextBox)gvRow.FindControl("txtUrunAdi")).Text;
                dr["Miktar"] = ((TextBox)gvRow.FindControl("txtMiktar")).Text;
                dr["Birim"] = ((DropDownList)gvRow.FindControl("ddlBirim")).Text;
                dr["BirimFiyat"] = ((TextBox)gvRow.FindControl("txtBirimFiyat")).Text;
                dr["ParaBirimi"] = ((Label)gvRow.FindControl("lblParaBirimi")).Text;
                dr["KDV"] = ((DropDownList)gvRow.FindControl("ddlKDV")).Text;
                dr["OTVGirisTuru"] = ((DropDownList)gvRow.FindControl("ddlotvGirisTuru")).Text;
                dr["Toplam"] = ((TextBox)gvRow.FindControl("txtToplam")).Text;
                dr["OTV"] = ((TextBox)gvRow.FindControl("txtOTV")).Text;
                dr["otvDurum"] = ((Label)gvRow.FindControl("lblotv")).Text;
                dr["OIV"] = ((TextBox)gvRow.FindControl("txtOIV")).Text;
                dr["oivDurum"] = ((Label)gvRow.FindControl("lbloiv")).Text;
            }
            dt.AcceptChanges();
        }
        protected void cmdDel_Click(object sender, EventArgs e)
        {
            SaveData();
            LinkButton cmdDel = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)cmdDel.Parent.Parent;

            dt.Rows[gvRow.RowIndex].Delete();
            dt.AcceptChanges();
            LoadGrid();
            GenelHesap();
        }
        protected void lbSatirEkle_Click(object sender, EventArgs e)
        {
            SaveData();
            AddRow();
            LoadGrid();
        }
        protected void lbOTVEkle_Click(object sender, EventArgs e)
        {
            SaveData();
            LinkButton cmdDel = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)cmdDel.Parent.Parent;

            ((Label)dgwUrunler.Rows[gvRow.RowIndex].FindControl("lblotv")).Text = "1";
            ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtOTV")).Visible = true;
            ((LinkButton)dgwUrunler.Rows[gvRow.RowIndex].FindControl("lbOtvKaldir")).Visible = true;
            pnlHesaplananOtv.Visible = true;
            ((DropDownList)dgwUrunler.Rows[gvRow.RowIndex].FindControl("ddlotvGirisTuru")).Visible = true;
        }
        protected void lbOIVEkle_Click(object sender, EventArgs e)
        {
            SaveData();
            LinkButton cmdDel = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)cmdDel.Parent.Parent;

            ((Label)dgwUrunler.Rows[gvRow.RowIndex].FindControl("lbloiv")).Text = "1";
            pnlHesaplananOiv.Visible = true;
            ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtOIV")).Visible = true;
            ((LinkButton)dgwUrunler.Rows[gvRow.RowIndex].FindControl("lbOivKaldir")).Visible = true;
            ((Label)dgwUrunler.Rows[gvRow.RowIndex].FindControl("yuzde")).Visible = true;
        }
        protected void dgwUrunler_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var dropdown = (DropDownList)e.Row.FindControl("ddlBirim");
                var dropdownkdv = (DropDownList)e.Row.FindControl("ddlKDV");
                var dropdownotv = (DropDownList)e.Row.FindControl("ddlotvGirisTuru");
                var txtOtv = (TextBox)e.Row.FindControl("txtOTV");
                var lbOtvKaldir = (LinkButton)e.Row.FindControl("lbOtvKaldir");
                var txtOiv = (TextBox)e.Row.FindControl("txtOIV");
                var lbOivKaldir = (LinkButton)e.Row.FindControl("lbOivKaldir");
                var lblOtv = (Label)e.Row.FindControl("lblotv");
                var lblOiv = (Label)e.Row.FindControl("lbloiv");
                var yuzde = (Label)e.Row.FindControl("yuzde");

                dropdown.SelectedValue = DataBinder.Eval(e.Row.DataItem, "Birim").ToString();//yeni satır eklendiği zaman seçili olan value değeri korunuyor
                dropdownkdv.SelectedValue = DataBinder.Eval(e.Row.DataItem, "KDV").ToString();
                dropdownotv.SelectedValue = DataBinder.Eval(e.Row.DataItem, "OTVGirisTuru").ToString();


                if (lblOtv.Text == "1")
                {
                    txtOtv.Visible = true;
                    dropdownotv.Visible = true;
                    lbOtvKaldir.Visible = true;
                    txtOtv.Text = DataBinder.Eval(e.Row.DataItem, "OTV").ToString();
                }
                else
                {
                    txtOtv.Visible = false;
                    dropdownotv.Visible = false;
                    lbOtvKaldir.Visible = false;
                }
                if (lblOiv.Text == "1")
                {
                    txtOiv.Visible = true;
                    lbOivKaldir.Visible = true;
                    yuzde.Visible = true;
                    txtOiv.Text = DataBinder.Eval(e.Row.DataItem, "OIV").ToString();
                }
                else
                {
                    txtOiv.Visible = false;
                    lbOivKaldir.Visible = false;
                    yuzde.Visible = false;
                }
            }
        }
        //---------------GridView Satır Ekleme İşlemleri----------------------------
        protected void OnTextChanged(object sender, EventArgs e)
        {

            TextBox textBox = sender as TextBox;
            GridViewRow gvRow = (GridViewRow)textBox.Parent.Parent;
            string id = textBox.ID;

            decimal gnltplm = 0;
            decimal fiyat = 0;
            int miktar = int.Parse(((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtMiktar")).Text);
            try
            {
                fiyat = decimal.Parse(((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtBirimFiyat")).Text);
            }
            catch (Exception)
            {

            }

            decimal sonuc = fiyat * miktar;
            if (id == "txtToplam")
            {
                lblDurum.Text = "toplam";
                decimal sonuct = decimal.Parse(((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtToplam")).Text) / miktar;
                int kdv = int.Parse(((DropDownList)dgwUrunler.Rows[gvRow.RowIndex].FindControl("ddlKDV")).SelectedValue);
                if (kdv == 0)
                {
                    gnltplm = sonuct;
                }
                else if (kdv == 1)
                {
                    gnltplm = sonuct / decimal.Parse("1,01");
                }
                else if (kdv == 8)
                {
                    gnltplm = sonuct / decimal.Parse("1,08");
                }
                else if (kdv == 18)
                {
                    gnltplm = sonuct / decimal.Parse("1,18");
                }

                ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtBirimFiyat")).Text = string.Format("{0:#,##0.0000}", double.Parse(gnltplm.ToString()));

                ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtToplam")).Text = string.Format("{0:#,##0.00}", double.Parse(((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtToplam")).Text));
            }
            else if (id == "txtMiktar")
            {
                if (lblDurum.Text == "toplam")
                {
                    decimal sonuct = decimal.Parse(((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtToplam")).Text) / miktar;
                    int kdv = int.Parse(((DropDownList)dgwUrunler.Rows[gvRow.RowIndex].FindControl("ddlKDV")).SelectedValue);
                    if (kdv == 0)
                    {
                        gnltplm = sonuct;
                    }
                    else if (kdv == 1)
                    {
                        gnltplm = sonuct / decimal.Parse("1,01");
                    }
                    else if (kdv == 8)
                    {
                        gnltplm = sonuct / decimal.Parse("1,08");
                    }
                    else if (kdv == 18)
                    {
                        gnltplm = sonuct / decimal.Parse("1,18");
                    }
                    ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtBirimFiyat")).Text = string.Format("{0:#,##0.00}", double.Parse(gnltplm.ToString()));
                    ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtToplam")).Text = string.Format("{0:#,##0.00}", double.Parse(((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtToplam")).Text));
                }
                else
                {
                    lblDurum.Text = "normal";
                    gnltplm = sonuc + (sonuc * int.Parse(((DropDownList)dgwUrunler.Rows[gvRow.RowIndex].FindControl("ddlKDV")).SelectedValue) / 100);
                    ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtBirimFiyat")).Text = string.Format("{0:#,##0.00}", double.Parse(fiyat.ToString()));
                    ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtToplam")).Text = string.Format("{0:#,##0.00}", double.Parse(gnltplm.ToString()));
                }
            }
            else
            {
                lblDurum.Text = "normal";
                gnltplm = sonuc + (sonuc * int.Parse(((DropDownList)dgwUrunler.Rows[gvRow.RowIndex].FindControl("ddlKDV")).SelectedValue) / 100);
                ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtBirimFiyat")).Text = string.Format("{0:#,##0.00}", double.Parse(fiyat.ToString()));
                ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtToplam")).Text = string.Format("{0:#,##0.00}", double.Parse(gnltplm.ToString()));
            }
            GenelHesap();
        }
        protected void SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            GridViewRow gvRow = (GridViewRow)ddl.Parent.Parent;
            string id = ddl.ID;

            decimal gnltplm = 0;
            decimal fiyat = 0;

            int miktar = int.Parse(((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtMiktar")).Text);
            try
            {
                fiyat = decimal.Parse(((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtBirimFiyat")).Text);
            }
            catch (Exception)
            {

            }
            if (lblDurum.Text == "toplam")
            {
                decimal sonuct = decimal.Parse(((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtToplam")).Text) / miktar;
                int kdv = int.Parse(((DropDownList)dgwUrunler.Rows[gvRow.RowIndex].FindControl("ddlKDV")).SelectedValue);

                if (kdv == 0)
                {
                    gnltplm = sonuct;
                }
                else if (kdv == 1)
                {
                    gnltplm = sonuct / decimal.Parse("1,01");
                }
                else if (kdv == 8)
                {
                    gnltplm = sonuct / decimal.Parse("1,08");
                }
                else if (kdv == 18)
                {
                    gnltplm = sonuct / decimal.Parse("1,18");
                }

                ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtBirimFiyat")).Text = string.Format("{0:#,##0.00}", double.Parse(gnltplm.ToString()));
                ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtToplam")).Text = string.Format("{0:#,##0.00}", double.Parse(((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtToplam")).Text));
            }
            else
            {
                decimal sonuc = fiyat * miktar;

                gnltplm = sonuc + (sonuc * int.Parse(((DropDownList)dgwUrunler.Rows[gvRow.RowIndex].FindControl("ddlKDV")).SelectedValue) / 100);
                ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtBirimFiyat")).Text = string.Format("{0:#,##0.00}", double.Parse(fiyat.ToString()));
                ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtToplam")).Text = string.Format("{0:#,##0.00}", double.Parse(gnltplm.ToString()));
            }
            GenelHesap();
        }
        protected void txtIskontoTutari_TextChanged(object sender, EventArgs e)
        {
            double otutar = double.Parse(lblOdenecekTutar.Text);
            double iskonto = double.Parse(txtIskontoTutari.Text);
            if (iskonto > otutar)
            {
                lblMesaj.Text = "İskonto tutarı <b><u>ödenecek tutardan</u></b> büyük olamaz.";
                popUp.Style.Add("background", "rgba(255,183,3,1)");
                popUp.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DuzenlePop", "mesaj();", true);
                basarili.Visible = false;
                uyari.Visible = true;
                basarisiz.Visible = false;
                txtIskontoTutari.Text = "0,00";
            }
            else
            {
                txtIskontoTutari.Text = string.Format("{0:#,##0.00}", double.Parse(txtIskontoTutari.Text));
                double geneltoplam = double.Parse(lblVergilerDahilTutar.Text) - double.Parse(txtIskontoTutari.Text);
                lblOdenecekTutar.Text = geneltoplam.ToString();
                lblOdenecekTutar.Text = string.Format("{0:#,##0.00}", double.Parse(lblOdenecekTutar.Text));
                txtTahsilEdilen.Text = lblOdenecekTutar.Text;
            }

        }
        protected void txtTahsilEdilen_TextChanged(object sender, EventArgs e)
        {
            txtTahsilEdilen.Text = string.Format("{0:#,##0.00}", double.Parse(txtTahsilEdilen.Text));
        }
        protected void ddlCariTipi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCariTipi.Text == "Yeni Cari")
            {
                txtCariUnvan.Visible = true;
                ddlCariler.Visible = false;
            }
            else
            {
                txtCariUnvan.Visible = false;
                ddlCariler.Visible = true;
            }
        }
        public void GenelHesap()
        {
            double miktar = 0;
            double birimFiyat = 0;
            double kdvOrani = 0;
            double satirToplam = 0;
            double satirToplamDoviz = 0;
            double otvSatirToplam = 0;
            double oivSatirToplam = 0;
            double kdvSatirToplam = 0;
            double urunTutari = 0;
            double malHizmetToplamTutar = 0;
            double hesaplananKDV = 0;
            double hesaplananOTV = 0;
            double hesaplananOIV = 0;
            double odenecekTutar = 0;
            double vergilerDahilTutar = 0;


            try
            {
                for (int i = 0; i < dgwUrunler.Rows.Count; ++i)
                {
                    string pBirimiUrun = ((Label)dgwUrunler.Rows[i].FindControl("lblParaBirimi")).Text;
                    if (((TextBox)dgwUrunler.Rows[i].FindControl("txtToplam")).Text != "")
                    {
                        miktar = Convert.ToDouble(((TextBox)dgwUrunler.Rows[i].FindControl("txtMiktar")).Text);

                        if (pBirimiUrun == "₺")
                        {
                            birimFiyat = Convert.ToDouble(((TextBox)dgwUrunler.Rows[i].FindControl("txtBirimFiyat")).Text);
                        }
                        else if (pBirimiUrun == "€")
                        {
                            birimFiyat = Convert.ToDouble(((TextBox)dgwUrunler.Rows[i].FindControl("txtBirimFiyat")).Text) * Convert.ToDouble(lblEUR.Text.Replace(".", ","));
                        }
                        else if (pBirimiUrun == "£")
                        {
                            birimFiyat = Convert.ToDouble(((TextBox)dgwUrunler.Rows[i].FindControl("txtBirimFiyat")).Text) * Convert.ToDouble(lblGBP.Text.Replace(".", ","));
                        }
                        else if (pBirimiUrun == "$")
                        {
                            birimFiyat = Convert.ToDouble(((TextBox)dgwUrunler.Rows[i].FindControl("txtBirimFiyat")).Text) * Convert.ToDouble(lblUSD.Text.Replace(".", ","));
                        }

                        urunTutari = miktar * birimFiyat;
                        malHizmetToplamTutar += urunTutari;
                        kdvOrani = Convert.ToDouble(((DropDownList)dgwUrunler.Rows[i].FindControl("ddlKDV")).SelectedValue);
                        if (((TextBox)dgwUrunler.Rows[i].FindControl("txtOTV")).Text != "")
                        {
                            if (((DropDownList)dgwUrunler.Rows[i].FindControl("ddlotvGirisTuru")).SelectedValue == "2")
                            {
                                otvSatirToplam = Convert.ToDouble(((TextBox)dgwUrunler.Rows[i].FindControl("txtOTV")).Text);
                            }
                            else
                            {
                                otvSatirToplam = (urunTutari * Convert.ToDouble(((TextBox)dgwUrunler.Rows[i].FindControl("txtOTV")).Text)) / 100;
                            }
                            if (((TextBox)dgwUrunler.Rows[i].FindControl("txtOIV")).Text != "")
                            {
                                oivSatirToplam = (urunTutari + otvSatirToplam) * Convert.ToDouble(((TextBox)dgwUrunler.Rows[i].FindControl("txtOIV")).Text) / 100;
                            }
                            kdvSatirToplam = (urunTutari + otvSatirToplam) * kdvOrani / 100;
                        }
                        else
                        {
                            if (((TextBox)dgwUrunler.Rows[i].FindControl("txtOIV")).Text != "")
                            {
                                oivSatirToplam = urunTutari * Convert.ToDouble(((TextBox)dgwUrunler.Rows[i].FindControl("txtOIV")).Text) / 100;
                            }
                            kdvSatirToplam = urunTutari * kdvOrani / 100;
                        }
                        satirToplam = urunTutari + kdvSatirToplam + otvSatirToplam + oivSatirToplam;
                        satirToplamDoviz = satirToplam;

                        hesaplananKDV += kdvSatirToplam;
                        hesaplananOTV += otvSatirToplam;
                        hesaplananOIV += oivSatirToplam;
                        vergilerDahilTutar += satirToplam;
                        otvSatirToplam = 0;
                        oivSatirToplam = 0;

                        double sonuc = 0;
                        if (pBirimiUrun == "₺")
                        {
                            ((TextBox)dgwUrunler.Rows[i].FindControl("txtToplam")).Text = string.Format("{0:#,##0.00}", double.Parse(satirToplamDoviz.ToString()));
                        }
                        else if (pBirimiUrun == "€")
                        {
                            sonuc = satirToplamDoviz / Convert.ToDouble(lblEUR.Text.Replace(".", ","));
                            ((TextBox)dgwUrunler.Rows[i].FindControl("txtToplam")).Text = string.Format("{0:#,##0.00}", double.Parse(sonuc.ToString()));
                        }
                        else if (pBirimiUrun == "£")
                        {
                            sonuc = satirToplamDoviz / Convert.ToDouble(lblGBP.Text.Replace(".", ","));
                            ((TextBox)dgwUrunler.Rows[i].FindControl("txtToplam")).Text = string.Format("{0:#,##0.00}", double.Parse(sonuc.ToString()));
                        }
                        else if (pBirimiUrun == "$")
                        {
                            sonuc = satirToplamDoviz / Convert.ToDouble(lblUSD.Text.Replace(".", ","));
                            ((TextBox)dgwUrunler.Rows[i].FindControl("txtToplam")).Text = string.Format("{0:#,##0.00}", double.Parse(sonuc.ToString()));
                        }

                    }
                }
                odenecekTutar = vergilerDahilTutar - double.Parse(txtIskontoTutari.Text);


                if (ddlParaBirimi.SelectedItem.Text == "USD")
                {
                    malHizmetToplamTutar /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    hesaplananOTV /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    hesaplananOIV /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    vergilerDahilTutar /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    hesaplananKDV /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    odenecekTutar /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                }
                else if (ddlParaBirimi.SelectedItem.Text == "EUR")
                {
                    malHizmetToplamTutar /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    hesaplananOTV /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    hesaplananOIV /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    vergilerDahilTutar /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    hesaplananKDV /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    odenecekTutar /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                }
                else if (ddlParaBirimi.SelectedItem.Text == "GBP")
                {
                    malHizmetToplamTutar /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    hesaplananOTV /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    hesaplananOIV /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    vergilerDahilTutar /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    hesaplananKDV /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                    odenecekTutar /= Convert.ToDouble(txtKur.Text.Replace(".", ","));
                }

                lblMalHizmetTutar.Text = string.Format("{0:#,##0.00}", malHizmetToplamTutar);
                lblHesaplananOtv.Text = string.Format("{0:#,##0.00}", hesaplananOTV);
                lblHesaplananOiv.Text = string.Format("{0:#,##0.00}", hesaplananOIV);
                lblVergilerDahilTutar.Text = string.Format("{0:#,##0.00}", vergilerDahilTutar);
                lblHesaplananKDV.Text = string.Format("{0:#,##0.00}", hesaplananKDV);
                lblOdenecekTutar.Text = string.Format("{0:#,##0.00}", odenecekTutar);
                txtTahsilEdilen.Text = lblOdenecekTutar.Text;


            }
            catch (Exception)
            {

            }
        }
        protected void otvHesap(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            GridViewRow gvRow = (GridViewRow)textBox.Parent.Parent;
            string otvt = ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtOTV")).Text;
            ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtOTV")).Text = string.Format("{0:#,##0.00}", double.Parse(otvt));
            GenelHesap();
        }
        protected void oivHesap(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            GridViewRow gvRow = (GridViewRow)textBox.Parent.Parent;
            string oivt = ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtOIV")).Text;
            ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtOIV")).Text = string.Format("{0:#,##0.00}", double.Parse(oivt));
            GenelHesap();
        }
        protected void lbOtvKaldir_Click(object sender, EventArgs e)
        {
            SaveData();
            LinkButton cmdDel = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)cmdDel.Parent.Parent;

            ((Label)dgwUrunler.Rows[gvRow.RowIndex].FindControl("lblotv")).Text = "0";
            ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtOTV")).Visible = false;
            ((LinkButton)dgwUrunler.Rows[gvRow.RowIndex].FindControl("lbOtvKaldir")).Visible = false;
            ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtOTV")).Text = "";
            for (int i = 0; i < dgwUrunler.Rows.Count; i++)
            {
                foreach (GridViewRow row in dgwUrunler.Rows)
                {
                    TextBox txtOtv = (TextBox)row.FindControl("txtOTV");
                    if (txtOtv.Visible == true)
                    {
                        pnlHesaplananOtv.Visible = true;
                        break;
                    }
                    else
                    {
                        pnlHesaplananOtv.Visible = false;
                    }
                }
            }
            ((DropDownList)dgwUrunler.Rows[gvRow.RowIndex].FindControl("ddlotvGirisTuru")).Visible = false;
            GenelHesap();
        }
        protected void lbOivKaldir_Click(object sender, EventArgs e)
        {
            SaveData();
            LinkButton cmdDel = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)cmdDel.Parent.Parent;

            ((Label)dgwUrunler.Rows[gvRow.RowIndex].FindControl("lbloiv")).Text = "0";
            ((Label)dgwUrunler.Rows[gvRow.RowIndex].FindControl("yuzde")).Visible = false;
            ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtOIV")).Visible = false;
            ((LinkButton)dgwUrunler.Rows[gvRow.RowIndex].FindControl("lbOivKaldir")).Visible = false;
            ((TextBox)dgwUrunler.Rows[gvRow.RowIndex].FindControl("txtOIV")).Text = "";
            for (int i = 0; i < dgwUrunler.Rows.Count; i++)
            {
                foreach (GridViewRow row in dgwUrunler.Rows)
                {
                    TextBox txtOiv = (TextBox)row.FindControl("txtOIV");
                    if (txtOiv.Visible == true)
                    {
                        pnlHesaplananOiv.Visible = true;
                        break;
                    }
                    else
                    {
                        pnlHesaplananOiv.Visible = false;
                    }
                }
            }
            GenelHesap();
        }
        protected void ddlotvGirisTuru_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenelHesap();
        }
        public String fnDoviz_Kurlari(String parIstenen_Kisim)
        {
            String strDonen_Deger = "";
            try
            {
                XmlTextReader xtrOkuyucu = new XmlTextReader("http://www.tcmb.gov.tr/kurlar/today.xml");
                XmlDocument xdDokuman = new XmlDocument();
                xdDokuman.Load(xtrOkuyucu);
                XmlNode xnDolar = xdDokuman.SelectSingleNode("/Tarih_Date/Currency[CurrencyName='US DOLLAR']");
                XmlNode xnEuro = xdDokuman.SelectSingleNode("/Tarih_Date/Currency[CurrencyName='EURO']");
                XmlNode xnSterlin = xdDokuman.SelectSingleNode("/Tarih_Date/Currency[CurrencyName='POUND STERLING']");
                XmlNode xnFrank = xdDokuman.SelectSingleNode("/Tarih_Date/Currency[CurrencyName='SWISS FRANK']");
                if (parIstenen_Kisim == "dolar_alis")
                {
                    strDonen_Deger = xnDolar.ChildNodes[3].InnerText;
                }
                else if (parIstenen_Kisim == "dolar_satis")
                {
                    strDonen_Deger = xnDolar.ChildNodes[4].InnerText;
                }
                else if (parIstenen_Kisim == "dolar_efektif_alis")
                {
                    strDonen_Deger = xnDolar.ChildNodes[5].InnerText;
                }
                else if (parIstenen_Kisim == "dolar_efektif_satis")
                {
                    strDonen_Deger = xnDolar.ChildNodes[6].InnerText;
                }

                else if (parIstenen_Kisim == "euro_alis")
                {
                    strDonen_Deger = xnEuro.ChildNodes[3].InnerText;
                }
                else if (parIstenen_Kisim == "euro_satis")
                {
                    strDonen_Deger = xnEuro.ChildNodes[4].InnerText;
                }
                else if (parIstenen_Kisim == "euro_efektif_alis")
                {
                    strDonen_Deger = xnEuro.ChildNodes[5].InnerText;
                }
                else if (parIstenen_Kisim == "euro_efektif_satis")
                {
                    strDonen_Deger = xnEuro.ChildNodes[6].InnerText;
                }

                else if (parIstenen_Kisim == "sterlin_alis")
                {
                    strDonen_Deger = xnSterlin.ChildNodes[3].InnerText;
                }
                else if (parIstenen_Kisim == "sterlin_satis")
                {
                    strDonen_Deger = xnSterlin.ChildNodes[4].InnerText;
                }
                else if (parIstenen_Kisim == "sterlin_efektif_alis")
                {
                    strDonen_Deger = xnSterlin.ChildNodes[5].InnerText;
                }
                else if (parIstenen_Kisim == "sterlin_efektif_satis")
                {
                    strDonen_Deger = xnSterlin.ChildNodes[6].InnerText;
                }
            }
            catch (Exception e)
            {

            }
            return strDonen_Deger;
        }
        protected void ddlParaBirimi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlParaBirimi.SelectedItem.Text == "TRY")
            {
                txtKur.Visible = false;
                txtKur.Text = "";
                GenelHesap();
            }
            else
            {
                txtKur.Visible = true;
                String strDolar = fnDoviz_Kurlari("dolar_alis");
                String strEuro = fnDoviz_Kurlari("euro_alis");
                String strSterlin = fnDoviz_Kurlari("sterlin_alis");
                if (ddlParaBirimi.SelectedItem.Text == "USD")
                {
                    txtKur.Text = strDolar;
                }
                else if (ddlParaBirimi.SelectedItem.Text == "EUR")
                {
                    txtKur.Text = strEuro;
                }
                else if (ddlParaBirimi.SelectedItem.Text == "GBP")
                {
                    txtKur.Text = strSterlin;
                }
                lblUSD.Text = strDolar;
                lblEUR.Text = strEuro;
                lblGBP.Text = strSterlin;
                GenelHesap();
            }
        }
    }
}