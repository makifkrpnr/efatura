﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="yeni-irsaliye.aspx.cs" Inherits="hesaptutar.com__Yeni_.yeni_irsaliye" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>hesaptutar.com | Yeni İrsaliye</title>
    <script type="text/javascript">
        function openModal() {
            $('#urunListesi').modal('show');
        }
        function closeModal() {
            $('#urunListesi').modal('close');
        }
    </script>
    <style>
        .sil-baslik{
            text-align:left!important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSiteMap" runat="server">
    <div class="breadcrumb-area">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/anasayfa">Genel Bakış</a></li>
                <li class="breadcrumb-item"><a href="/muhasebe">Muhasebe</a></li>
                <li class="breadcrumb-item active" aria-current="page">Yeni İrsaliye</li>
            </ol>
        </nav>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content yeni-musteri">
        <div class="container">
            <div class="row cnt">
                <div class="col-lg-12">
                    <span class="form-baslik mb-3">İrsaliye Bilgileri</span>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="simpleinput">İrsaliye No</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:TextBox ID="txtIrsaliyeNo" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">Cari Tipi</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:DropDownList ID="ddlCariTipi" class="form-select" runat="server" AutoPostBack="true">
                                                <asp:ListItem>Kayıtlı Cari</asp:ListItem>
                                                <asp:ListItem>Yeni Cari</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">Ödeme Şekli</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:DropDownList ID="ddlOdemeSekli" class="form-select" runat="server">
                                                <asp:ListItem>Nakit</asp:ListItem>
                                                <asp:ListItem>Çek</asp:ListItem>
                                                <asp:ListItem>Açık Hesap</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">İl</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:DropDownList ID="ddlIl" AutoPostBack="true" class="js-example-basic-single form-select" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="simpleinput">İrsaliye Tarihi</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:TextBox ID="txtIrsaliyeTarihi" type="datetime-local" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">Cari Ünvanı</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:DropDownList ID="ddlCariler" AutoPostBack="true" class="form-select" runat="server"></asp:DropDownList>
                                            <asp:TextBox ID="txtCariUnvan" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">Para Birimi</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:DropDownList ID="ddlParaBirimi" class="form-select" runat="server">
                                                <asp:ListItem>TRY</asp:ListItem>
                                                <asp:ListItem>USD</asp:ListItem>
                                                <asp:ListItem>EUR</asp:ListItem>
                                                <asp:ListItem>GBP</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">İlçe</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:DropDownList ID="ddlIlce" class="js-example-basic-single form-select" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="simpleinput">Vergi Dairesi</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:TextBox ID="txtVergiDairesi" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="simpleinput">Vergi Numarası</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:TextBox ID="txtVergiNo" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="simpleinput">Adres</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:TextBox ID="txtAdres" TextMode="MultiLine" Rows="1" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="simpleinput">Açıklama</label>
                                        <div class="col-lg-8 mb-3">
                                            <asp:TextBox ID="txtAciklama" TextMode="MultiLine" Style="height: 90px; resize: none;" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <asp:Label ID="lblIndis" runat="server" Visible="false" Text="Label"></asp:Label>
                            <asp:Label ID="lblStokId" runat="server" Visible="false" Text="Label"></asp:Label>
                            <asp:Label ID="lblDurum" runat="server" Visible="false" Text="normal"></asp:Label>


                            <div class="row contact-person">
                                <asp:GridView ID="dgwUrunler" runat="server" CssClass="table table-sm mb-0 no-plc fatura-urunler" BorderStyle="None" OnRowDataBound="dgwUrunler_RowDataBound" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Ürün / Hizmet">
                                            <ItemTemplate>
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtUrunAdi" CssClass="form-control" Text='<%# Eval("UrunAdi") %>' runat="server"></asp:TextBox>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-outline-secondary"><i class="fas fa-search"></i></asp:LinkButton>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Miktar">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtMiktar" AutoCompleteType="Disabled" CssClass="form-control" Text='<%# Eval("Miktar") %>' onkeypress="return SayiGirme(event)" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Birim">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlBirim" CssClass="form-select" runat="server">
                                                    <asp:ListItem>Adet</asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-CssClass="sil-baslik" HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="cmdDel_Click"><span class="satir-sil"><i class="fas fa-times"></i></span></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>

                            <div class="input-group mt-3">
                                <asp:LinkButton ID="lbSatirEkle" CssClass="btn btn-danger" runat="server" OnClick="lbSatirEkle_Click"><span><i class="fas fa-plus"></i></span> Satır Ekle</asp:LinkButton>

                            </div>

                            <div class="d-grid gap-2 d-md-flex mt-5 justify-content-end">

                                <div>
                                    <button class="btn btn-primary me-md-2" type="button" onclick="javascript:history.go(-1)">Vazgeç</button>
                                    <div class="btn-group">
                                        <asp:Button ID="btnKaydet" CssClass="btn btn-danger" runat="server" Text="Kaydet" ValidationGroup="faturabil" />

                                        <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                                            <span class="visually-hidden">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <asp:LinkButton ID="lbKaydetveYeni" CssClass="dropdown-item" runat="server" ValidationGroup="faturabil">Kaydet ve Yeni Ekle</asp:LinkButton>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnKaydet" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>
    <script>
        function pageLoad() {
            $('.js-example-basic-single').select2();
        }
    </script>
    <script>
        function mesaj() {
            $(document).ready(function () {
                setTimeout(function () {
                    $(".popup").fadeOut("slow", function () {
                        $(".popup").remove();
                    });
                }, 3000);
            });
        }
    </script>
    <!-- Datatables -->
    <script>
        $.extend($.fn.dataTable.defaults, {
            responsive: true
        });
        $(document).ready(function () {
            $('#urunList').DataTable({
                dom: 'lfrtip',
                "pageLength": 25,
            });
        });
    </script>
</asp:Content>
